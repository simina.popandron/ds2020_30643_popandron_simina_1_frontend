import React, {useState} from 'react';
import ApiLogin from '../API/UserAPI';
import Redirect from "react-router-dom/Redirect";
import {getIsLoggedInFromLocalStorage, setIsLoggedInLocalStorage,setRoleInLocalStorage, getRoleFromLocalStorage} from '../Helpers';
import FormGroup from 'react-bootstrap/FormGroup';
import FormControl from 'react-bootstrap/FormControl';
import {Button} from 'react-bootstrap';


export default function Login() {
    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');
    const [role] = useState('');
    let redirect;

    const isLoggedIn = getIsLoggedInFromLocalStorage();
    const activeRole = getRoleFromLocalStorage();

    if (isLoggedIn === 'true') {
        if(activeRole==='doctor') {
            redirect = <Redirect push to="/doctorHome"/>
        }
        if(activeRole==='patient') {
            redirect = <Redirect push to="/patientHome"/>
        }
        if(activeRole==='caregiver') {
            redirect = <Redirect push to="/caregiverHome"/>
        }
    }
    else{
        redirect='';
    }

    console.log ("is logged in:   ",getIsLoggedInFromLocalStorage());
    console.log("role:  ",getRoleFromLocalStorage());

    function handleLoginSubmit(event) {
        let logindata = {
            username: username,
            password: password,
            role: role
        }

        ApiLogin.login(logindata)
            .then(response => {
                localStorage.setItem('id', response.data.id);
                localStorage.setItem('role', response.data.role);

                setIsLoggedInLocalStorage('true');
                setRoleInLocalStorage(localStorage.getItem('role'));

                console.log(localStorage.getItem('id'));
                console.log(localStorage.getItem('role'));
                console.log(getRoleFromLocalStorage());
                window.location.reload(false);
            })
            .catch(error => {
                alert('username/password incorrect');
            });

        //console.log("whatever");
        console.log(localStorage.getItem('id'));
        event.preventDefault();
    }
    function validateForm() {
        return password.length > 0 && username.length > 0;
    }


        return (
            <div className="container-fluid page_background_color full text-center container portfolio">
                <div className="wrapper fadeInDown">
                    <div id="formContent">
                        <form onSubmit={handleLoginSubmit}>
                            <FormGroup controlId="username" bssize="large">
                                <label>Username</label>
                                <FormControl
                                    autoFocus
                                    type="username"
                                    value={username}
                                    onChange={e => setUsername(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup controlId="password" bssize="large">
                                <label>Parola</label>
                                <FormControl
                                    type="password"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                />
                            </FormGroup>
                            <Button block bssize="large" disabled={!validateForm()} type="submit" >
                                Login
                            </Button>
                        </form>
                        {redirect}
                    </div>
                </div>
            </div>
        );


}
