import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Login from "./Login/login";
import DoctorHome from "./Doctor/doctorHome"
import PatientHome from "./Patient/patientHome"
import CaregiverHome from "./Caregiver/caregiverHome"
import ViewPatientList from "./Doctor/viewPatientList"
import ViewCaregiverList from "./Doctor/viewCaregiverList"
import ViewMedicationList from "./Doctor/viewMedicationList"
import ViewAllPatients from "./Caregiver/viewAllPatients"

import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/login'
                            render={() => <Login/>}
                        />

                        <Route
                            exact
                            path='/doctorHome'
                            render={() => <DoctorHome/>}
                        />


                        <Route
                            exact
                            path='/patientHome'
                            render={() => <PatientHome/>}
                        />

                        <Route
                            exact
                            path='/caregiverHome'
                            render={() => <CaregiverHome/>}
                        />


                        <Route
                            exact
                            path='/doctor/viewPatientList'
                            render={() => <ViewPatientList/>}
                        />

                        <Route
                            exact
                            path='/doctor/viewCaregiverList'
                            render={() => <ViewCaregiverList/>}
                        />

                        <Route
                            exact
                            path='/doctor/viewMedicationList'
                            render={() => <ViewMedicationList/>}
                        />
                        <Route
                            exact
                            path='/caregiver/viewAllPatients'
                            render={() => <ViewAllPatients/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />


                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
