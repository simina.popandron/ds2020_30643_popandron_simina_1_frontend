import axios from 'axios'

class PatientAPI {
    insertPatient(patient) {
        return axios.post(`http://localhost:8080/rest/patient`, patient);
    }

    getPatient() {
        return axios.get(`http://localhost:8080/rest/patient`);
    }

    deletePatient(id) {
        return axios.delete(`http://localhost:8080/rest/patient/` + id);
    }

    updatePatient(patient) {
        return axios.put(`http://localhost:8080/rest/patient`, patient);
    }
}
export default new PatientAPI()