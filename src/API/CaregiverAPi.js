import axios from 'axios'

class CaregiverAPi {
 /*   login(data) {
        return axios.post(`http://localhost:8080/rest/login`, data);
    }
*/
    insertCaregiver(caregiver) {
        return axios.post(`http://localhost:8080/rest/caregiver`, caregiver);
    }
    getCaregiver() {
        return axios.get(`http://localhost:8080/rest/caregiver`);
    }

    updateCaregiver(caregiver) {
        return axios.put(`http://localhost:8080/rest/caregiver`, caregiver);
    }
    deleteCaregiver(id) {
        return axios.delete(`http://localhost:8080/rest/caregiver/` + id);
    }

    viewPatientsCaregiver(id){
        return axios.get(`http://localhost:8080/rest/caregiver/` + id);
    }
}
export default new CaregiverAPi()