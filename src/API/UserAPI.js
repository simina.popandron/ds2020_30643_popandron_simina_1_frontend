import axios from 'axios'

class ApiService {
    login(data) {
        return axios.post(`http://localhost:8080/rest/login`, data);
    }
}
export default new ApiService()