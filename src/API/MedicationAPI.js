import axios from 'axios'

class MedicationAPI {
    insertMedication(medication){
        return axios.post(`http://localhost:8080/rest/medication`, medication);
    }
    getMedication(){
        return axios.get(`http://localhost:8080/rest/medication`);
    }
    deleteMedication(id){
        return axios.delete(`http://localhost:8080/rest/medication/` + id);
    }
    updateMedication(medication){
        return axios.put(`http://localhost:8080/rest/medication`, medication);
    }
    insertIntake(intake) {
        return axios.post(`http://localhost:8080/rest/intake`, intake);
    }
}
export default new MedicationAPI()
