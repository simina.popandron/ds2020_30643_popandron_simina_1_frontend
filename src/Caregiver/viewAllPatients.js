import React from 'react'
import {getRoleFromLocalStorage} from "../Helpers";
import CaregiverAPi from "../API/CaregiverAPi";

class viewAllPatients extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data:[],
            id: localStorage.getItem('id'),
        };
        //console.log("ID:  ",localStorage.getItem('id'));
    }

    componentDidMount() {
        this.handleViewPatients();
    }

    /*handleViewPatients = () => {
        PatientAPI.getPatient()
            .then(response => this.setState({data:response.data},
                console.log(response.data)
            ))
            .catch(e => console.log(e));
    };*/

    handleViewPatients = () => {
        let id=localStorage.getItem('id')
        console.log("IDhandle:  ",id);
        CaregiverAPi.viewPatientsCaregiver(id)
            .then(response => this.setState({data: response.data},
                console.log(response.data)
            ))
            .catch(e => console.log(e));
    }

    render()
        {

            if (getRoleFromLocalStorage() === 'caregiver') {
                return (
                    <div className="container">
                        <h1 id='title'> Your patients are: </h1>
                        <table id='patients'>
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>BirthDate</th>
                                <th>Gender</th>
                                <th>Address</th>
                                <th>MedicalRecord</th>
                                <th>idCaregiver</th>
                            </tr>
                            </thead>
                            <tbody>
                            {

                                this.state.data.map(patient => {
                                    return (<tr key={patient.id}>
                                            <td>{patient.id}</td>
                                            <td>{patient.name}</td>
                                            <td>{patient.birthDate.substring(0, 10)}</td>
                                            <td>{patient.gender}</td>
                                            <td>{patient.address}</td>
                                            <td>{patient.medicalRecord}</td>
                                            <td>{patient.idCaregiver}</td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                        <br/>
                    </div>

                );
            } else {
                return (
                    <div>
                        <h3>You are not a caregiver</h3>
                        <div>Page not found</div>
                    </div>
                )
            }

        }
}
export default viewAllPatients;