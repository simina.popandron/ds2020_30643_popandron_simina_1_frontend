import React from 'react'
import Button from "react-bootstrap/Button";
import MedicationAPI from "../API/MedicationAPI";
import validate from "../validators/person-validators";
import {getRoleFromLocalStorage} from "../Helpers";

class viewMedicationList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data:[],

            name: this.props.name,
            placeholder: this.props.placeholder,
            value: this.props.value,
            onChange: this.props.handleChange,
            valid: this.props.valid,

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {
                id: {
                    value: '',
                    placeholder: ' ',
                    valid: true,

                },

                name: {
                    value: '',
                    placeholder: ' ',
                    valid: false,
                    validationRules: {
                        minLength: 5,
                        isRequired: true
                    }
                },

                dosage: {
                    value: '',
                    placeholder: 'Number',
                    valid: false,
                    validationRules: {
                        minLength: 1,
                        isRequired: true
                    }
                },

                sideEffects: {
                    value: '',
                    placeholder: '',
                    validationRules: {
                        minLength: 1,
                        isRequired: true
                    }
                },
            }
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitMedication = this.handleSubmitMedication.bind(this);
        this.updateMedication = this.updateMedication.bind(this);
    }

    componentDidMount() {
        this.handleViewMedication();
    }

    handleViewMedication = () => {
        MedicationAPI.getMedication()
            .then(response => this.setState({data:response.data},
                console.log(response.data)
            ))
            .catch(e => console.log(e));
    };

    handleMedicationDeleteClick = id => {
        MedicationAPI.deleteMedication(id)
            .then(response => {
                console.log("Delete done for Medication: ", id);
                window.location.reload(false);
            })
            .catch(e => console.log("Patient delete error"));
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    handleSubmitMedication(){
        let medication = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            dosage: this.state.formControls.dosage.value,
            sideEffects: this.state.formControls.sideEffects.value
        };
        MedicationAPI.insertMedication(medication);
    }

    updateMedication() {
        let medication = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            dosage: this.state.formControls.dosage.value,
            sideEffects: this.state.formControls.sideEffects.value,
        };
        MedicationAPI.updateMedication(medication);
        window.location.reload(false);
    }

    render() {

        let formControl = "form-control";

        if (this.props.touched && !this.props.valid) {
            formControl = 'form-control control-error';
        }

        if (getRoleFromLocalStorage() === 'doctor') {
            return (
                <div className="container">
                    <h1 id='title'>View All Patients</h1>
                    <table id='patients'>
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>SideEffects</th>
                            <th>Dosage</th>
                            <th>Delete item?</th>
                        </tr>
                        </thead>
                        <tbody>
                        {

                            this.state.data.map(medication => {
                                return (<tr key={medication.id}>
                                        <td>{medication.id}</td>
                                        <td>{medication.name}</td>
                                        <td>{medication.sideEffects}</td>
                                        <td>{medication.dosage}</td>
                                        <td>
                                            <Button
                                                onClick={() => this.handleMedicationDeleteClick(medication.id)}
                                            >
                                                Delete
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                    <br/>


                    <form onSubmit={this.handleSubmitMedication}>

                        <h1>Update/Add new Medication</h1>

                        <p> Id: </p>
                        <div className="form-group">
                            <input name="id" type="text" className={formControl}
                                   placeholder={this.state.formControls.id.placeholder}
                                   value={this.state.formControls.id.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.id.valid}/>
                        </div>

                        <p> Name: </p>
                        <div className="form-group">
                            <input name="name" type="text" className={formControl}
                                   placeholder={this.state.formControls.name.placeholder}
                                   value={this.state.formControls.name.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.name.valid}
                            />
                        </div>

                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> Dosage: </p>
                        <div className="form-group">
                            <input name="dosage" type="text" className={formControl}
                                   placeholder={this.state.formControls.dosage.placeholder}
                                   value={this.state.formControls.dosage.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.dosage.valid}
                            />
                        </div>

                        {this.state.formControls.dosage.touched && !this.state.formControls.dosage.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> Side Effects: </p>
                        <div className="form-group">
                            <input name="sideEffects" type="text" className={formControl}
                                   placeholder={this.state.formControls.sideEffects.placeholder}
                                   value={this.state.formControls.sideEffects.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.sideEffects.valid}
                            />
                        </div>

                        {this.state.formControls.sideEffects.touched && !this.state.formControls.sideEffects.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}


                        <p></p>
                        <Button variant="success"
                                type={"submit"}
                                disabled={!this.state.formIsValid}>
                            Submit
                        </Button>

                        &nbsp;
                        <Button variant="success" onClick={() =>
                            this.updateMedication()}>
                            Update
                        </Button>

                    </form>

                </div>

            );
        }
        else{
            return(
                <div>
                    <h3>You are not a doctor</h3>
                    <div>Page not found</div>
                </div>
            )
        }
    }

}

export default viewMedicationList;