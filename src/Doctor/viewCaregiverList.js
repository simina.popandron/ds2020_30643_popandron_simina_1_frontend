import React from 'react'
import Button from "react-bootstrap/Button";
import CaregiverAPi from "../API/CaregiverAPi";
import validate from "../validators/person-validators";
import {getRoleFromLocalStorage} from "../Helpers";

const ElementStyle = {
    backgroundColor: '#f7786b',
    width:100,
};

class viewCaregiverList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data:[],

            name: this.props.name,
            placeholder: this.props.placeholder,
            value: this.props.value,
            onChange: this.props.handleChange,
            valid: this.props.valid,

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {
                id: {
                    value: '',
                    placeholder: '',
                    valid: true,

                },

                name: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                birthDate: {
                    value: '',
                    placeholder: 'yyyy-mm-dd',
                    valid: false,
                    validationRules: {
                        isRequired: true
                    }
                },

                gender: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                address: {
                    value: '',
                    placeholder: 'Street & Number',
                    valid: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                username: {
                    value: '',
                    placeholder: '',
                    valid: true,
                    validationRules: {
                        isRequired: true
                    }
                },
                password: {
                    value: '',
                    placeholder: '',
                    valid: true,
                    validationRules: {
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitCaregiver = this.handleSubmitCaregiver.bind(this);
        this.updateCaregiver = this.updateCaregiver.bind(this);

    }


    componentDidMount() {
        this.handleViewCaregivers();
    }

    handleViewCaregivers = () => {
        CaregiverAPi.getCaregiver()
            .then(response => this.setState({data:response.data},
                console.log(response.data)
            ))
            .catch(e => console.log(e));
    };

    handleCaregiverDeleteClick = id => {
        CaregiverAPi.deleteCaregiver(id)
            .then(response => {
                console.log("Delete done for Patient: ", id);
                window.location.reload(false);

            })
            .catch(e => console.log("Patient delete error"));
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    updateCaregiver() {
        let caregiver = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value
        };
        CaregiverAPi.updateCaregiver(caregiver);
        window.location.reload(false);
    }
    handleSubmitCaregiver(){
        let caregiver = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value
        };
        CaregiverAPi.insertCaregiver(caregiver);
    }



    render() {

        let formControl = "form-control";

        if (this.props.touched && !this.props.valid) {
            formControl = 'form-control control-error';
        }

        if (getRoleFromLocalStorage() === 'doctor') {
            return (
                <div className="container">
                    <h1 id='title'>Caregivers</h1>
                    <table id='patients'>
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>BirthDate</th>
                            <th>Gender</th>
                            <th>Address</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Operations</th>
                        </tr>
                        </thead>
                        <tbody>
                        {

                            this.state.data.map(caregiver => {
                                return (<tr key={caregiver.id}>
                                        <td>{caregiver.id}</td>
                                        <td>{caregiver.name}</td>
                                        <td>{caregiver.birthDate.substring(0, 10)}</td>
                                        <td>{caregiver.gender}</td>
                                        <td>{caregiver.address}</td>
                                        <td>{caregiver.username}</td>
                                        <td>{caregiver.password}</td>
                                        <td>
                                            <Button style={ElementStyle}
                                                    onClick={() => this.handleCaregiverDeleteClick(caregiver.id)}
                                            >
                                                Delete
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                    <br/>


                    <form onSubmit={this.handleSubmitCaregiver}>

                        <h1>Insert/Update Caregiver</h1>

                        <p> Id: </p>

                        <div className="form-group">
                            <input name="id" type="text" className={formControl}
                                   placeholder={this.state.formControls.id.placeholder}
                                   value={this.state.formControls.id.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.id.valid}
                            />
                        </div>

                        <p> Name: </p>
                        <div className="form-group">
                            <input name="name" type="text" className={formControl}
                                   placeholder={this.state.formControls.name.placeholder}
                                   value={this.state.formControls.name.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.name.valid}
                            />
                        </div>

                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> Birth date: </p>
                        <div className="form-group">
                            <input name="birthDate" type="text" className={formControl}
                                   placeholder={this.state.formControls.birthDate.placeholder}
                                   value={this.state.formControls.birthDate.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.birthDate.valid}
                            />
                        </div>

                        {this.state.formControls.birthDate.touched && !this.state.formControls.birthDate.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> Gender: </p>
                        <div className="form-group">
                            <input name="gender" type="text" className={formControl}
                                   placeholder={this.state.formControls.gender.placeholder}
                                   value={this.state.formControls.gender.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.gender.valid}
                            />
                        </div>

                        {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> Address: </p>
                        <div className="form-group">
                            <input name="address" type="text" className={formControl}
                                   placeholder={this.state.formControls.address.placeholder}
                                   value={this.state.formControls.address.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.address.valid}
                            />
                        </div>

                        {this.state.formControls.address.touched && !this.state.formControls.address.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}


                        <p> username: </p>
                        <div className="form-group">
                            <input name="username" type="text" className={formControl}
                                   placeholder={this.state.formControls.username.placeholder}
                                   value={this.state.formControls.username.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.username.valid}
                            />
                        </div>


                        <p> password: </p>
                        <div className="form-group">
                            <input name="password" type="text" className={formControl}
                                   placeholder={this.state.formControls.password.placeholder}
                                   value={this.state.formControls.password.value}
                                   onChange={this.handleChange}
                                   valid={this.state.formControls.password.valid}
                            />
                        </div>


                        <p></p>
                        <Button variant="success"
                                type={"submit"}
                                disabled={!this.state.formIsValid}>
                            Add
                        </Button>

                        &nbsp;
                        <Button variant="success" onClick={() => this.updateCaregiver()}>
                            Update
                        </Button>

                        {this.state.errorStatus > 0}


                    </form>

                </div>

            );
        }
        else{
            return(
                <div>
                    <h3>You are not a doctor</h3>
                    <div>Page not found</div>
                </div>
            )
        }
    }

}
export default viewCaregiverList;