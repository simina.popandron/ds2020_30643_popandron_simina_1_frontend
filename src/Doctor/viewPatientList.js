import React from 'react'
import Button from "react-bootstrap/Button";
import PatientAPI from '../API/PatientAPI';
import {getRoleFromLocalStorage} from "../Helpers";
import validate from "../validators/person-validators";

const ElementStyle = {
    backgroundColor: '#f7786b',
    width:100,
};

class viewPatientList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data:[],

            name: this.props.name,
            placeholder: this.props.placeholder,
            value: this.props.value,
            onChange: this.props.handleChange,
            touched: this.props.touched,
            valid: this.props.valid,

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {
                id: {
                    value: '',
                    placeholder: '',
                    valid: true,
                    touched: false,

                },

                name: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                birthDate: {
                    value: '',
                    placeholder: 'YYYY-MM-DD',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

                gender: {
                    value: '',
                    placeholder: 'M/F',
                    valid: false,
                    touched: false,

                },
                address: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                medicalRecord: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                username: {
                    value: '',
                    placeholder: 'username',
                    valid: true,
                    touched: false,
                },
                password: {
                    value: '',
                    placeholder: 'password',
                    valid: true,
                    touched: false,
                },
                idCaregiver: {
                    value: '',
                    placeholder: 'ID',
                    valid: true,
                    touched: false,
                },
            }

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitPatient = this.handleSubmitPatient.bind(this);
        this.updatePatient = this.updatePatient.bind(this);
        this.handlePatientDeleteClick=this.handlePatientDeleteClick.bind(this);
    }


    componentDidMount() {
        this.handleViewPatients();
    }

    handleViewPatients = () => {
        PatientAPI.getPatient()
            .then(response => this.setState({data:response.data},
                console.log(response.data)
            ))
            .catch(e => console.log(e));
    };

    handlePatientDeleteClick = id => {
        PatientAPI.deletePatient(id)
            .then(response => {
                //this.refreshPatient();
                console.log("Delete done for Patient: ", id);
                window.location.reload(false);

            })
            .catch(e => console.log("Patient delete error"));
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };


    updatePatient() {
        let patient = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
            medicalRecord: this.state.formControls.medicalRecord.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            idCaregiver: this.state.formControls.idCaregiver.value
        };
        PatientAPI.updatePatient(patient);
        window.location.reload(false);
    }
    handleSubmitPatient(){
        let patient = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
            medicalRecord: this.state.formControls.medicalRecord.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            idCaregiver: this.state.formControls.idCaregiver.value
        };
        PatientAPI.insertPatient(patient);
    }


    render() {

        let formControl = "form-control";

        if (this.props.touched && !this.props.valid) {
            formControl = 'form-control control-error';
        }

        if (getRoleFromLocalStorage() === 'doctor') {
            return (
                <div className="container">
                    <h1 id='title'> Patients</h1>
                    <table id='patients'>
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>BirthDate</th>
                            <th>Gender</th>
                            <th>Address</th>
                            <th>MedicalRecord</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>idCaregiver</th>
                            <th>Operations</th>
                        </tr>
                        </thead>
                        <tbody>
                        {

                            this.state.data.map(patient => {
                                return (<tr key={patient.id}>
                                        <td>{patient.id}</td>
                                        <td>{patient.name}</td>
                                        <td>{patient.birthDate.substring(0, 10)}</td>
                                        <td>{patient.gender}</td>
                                        <td>{patient.address}</td>
                                        <td>{patient.medicalRecord}</td>
                                        <td>{patient.username}</td>
                                        <td>{patient.password}</td>
                                        <td>{patient.idCaregiver}</td>
                                        <td>
                                            <Button style={ElementStyle}
                                                    onClick={() => this.handlePatientDeleteClick(patient.id)}
                                            >
                                                Delete
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                    <br/>

                    <form onSubmit={this.handleSubmitPatient}>

                        <h1>Insert/Update Patient</h1>

                        <p> Id: </p>
                        <div className="form-group">
                            <input name="id" type="text" className={formControl}
                                   placeholder={this.state.formControls.id.placeholder}
                                   value={this.state.formControls.id.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.id.touched}
                                   valid={this.state.formControls.id.valid}
                            />
                        </div>

                        <p> Name: </p>
                        <div className="form-group">
                            <input name="name" type="text" className={formControl}
                                   placeholder={this.state.formControls.name.placeholder}
                                   value={this.state.formControls.name.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.name.touched}
                                   valid={this.state.formControls.name.valid}
                            />
                        </div>

                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> Birth date: </p>
                        <div className="form-group">
                            <input name="birthDate" type="text" className={formControl}
                                   placeholder={this.state.formControls.birthDate.placeholder}
                                   value={this.state.formControls.birthDate.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.birthDate.touched}
                                   valid={this.state.formControls.birthDate.valid}
                            />
                        </div>

                        {this.state.formControls.birthDate.touched && !this.state.formControls.birthDate.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> Gender: </p>
                        <div className="form-group">
                            <input name="gender" type="text" className={formControl}
                                   placeholder={this.state.formControls.gender.placeholder}
                                   value={this.state.formControls.gender.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.gender.touched}
                                   valid={this.state.formControls.gender.valid}
                            />
                        </div>

                        {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> Address: </p>
                        <div className="form-group">
                            <input name="address" type="text" className={formControl}
                                   placeholder={this.state.formControls.address.placeholder}
                                   value={this.state.formControls.address.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.address.touched}
                                   valid={this.state.formControls.address.valid}
                            />
                        </div>

                        {this.state.formControls.address.touched && !this.state.formControls.address.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> Medical record: </p>
                        <div className="form-group">
                            <input name="medicalRecord" type="text" className={formControl}
                                   placeholder={this.state.formControls.medicalRecord.placeholder}
                                   value={this.state.formControls.medicalRecord.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.medicalRecord.touched}
                                   valid={this.state.formControls.medicalRecord.valid}
                            />
                        </div>

                        {this.state.formControls.medicalRecord.touched && !this.state.formControls.medicalRecord.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                        <p> username: </p>
                        <div className="form-group">
                            <input name="username" type="text" className={formControl}
                                   placeholder={this.state.formControls.username.placeholder}
                                   value={this.state.formControls.username.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.username.touched}
                                   valid={this.state.formControls.username.valid}
                            />
                        </div>

                        <p> password: </p>
                        <div className="form-group">
                            <input name="password" type="text" className={formControl}
                                   placeholder={this.state.formControls.password.placeholder}
                                   value={this.state.formControls.password.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.password.touched}
                                   valid={this.state.formControls.password.valid}
                            />
                        </div>

                        <p> caregiver id: </p>
                        <div className="form-group">
                            <input name="idCaregiver" type="text" className={formControl}
                                   placeholder={this.state.formControls.idCaregiver.placeholder}
                                   value={this.state.formControls.idCaregiver.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.idCaregiver.touched}
                                   valid={this.state.formControls.idCaregiver.valid}
                            />
                        </div>


                        <p> </p>

                        <Button variant="success"
                                type={"submit"}
                                disabled={!this.state.formIsValid}>
                            Add
                        </Button>
                        &nbsp;
                        <Button variant="success" onClick={() => this.updatePatient()}>
                            Update
                        </Button>


                    </form>

                </div>

            );
        }
        else{
            return(
                <div>
                    <h3>You are not a doctor</h3>
                    <div>Page not found</div>
                </div>
            )
        }
    }

}
export default viewPatientList;