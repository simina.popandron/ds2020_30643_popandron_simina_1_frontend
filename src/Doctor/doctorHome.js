import React from 'react'

import BackgroundImg from '../commons/images/future-medicine.jpg';

import { Container, Jumbotron} from 'reactstrap';
import {getRoleFromLocalStorage} from "../Helpers";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };
class doctorHome extends React.Component{
    render() {
        if (getRoleFromLocalStorage() === 'doctor') {
            return (
                <div>
                    <Jumbotron fluid style={backgroundStyle}>
                        <Container fluid>
                            <h1 className="display-3" style={textStyle}>Hello Doctor!</h1>
                            <p className="lead" style={textStyle}><b></b></p>
                        </Container>
                    </Jumbotron>
                </div>
            )
        }
        else{
            return(
                <div>
                    <h3>You are not a doctor</h3>
                    <div>Page not found</div>
                </div>
            )
        }
    }
}
export default doctorHome;