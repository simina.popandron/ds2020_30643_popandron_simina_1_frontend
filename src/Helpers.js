export function setIsLoggedInLocalStorage(isLoggedIn){
    return localStorage.setItem('isLoggedIn', isLoggedIn);
}

export function getIsLoggedInFromLocalStorage(){
    return localStorage.getItem('isLoggedIn');
}

export function getId(id){
    return localStorage.getItem(id);
}

export function setRoleInLocalStorage(activeRole){
    return localStorage.setItem('activeRole', activeRole);
}

export function getRoleFromLocalStorage(){
    return localStorage.getItem('activeRole');
}