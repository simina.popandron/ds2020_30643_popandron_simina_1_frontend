/*import React from 'react'
import logo from './commons/images/icon.png';
import NavItem from "react-bootstrap/NavItem";

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};



const NavigationBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                       Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/persons">Persons</NavLink>
                        </DropdownItem>z

                    </DropdownMenu>
                </UncontrolledDropdown>
                <NavItem >
                    <NavLink href="/login" className="text_navbar" >Login</NavLink>
                </NavItem>
            </Nav>
        </Navbar>
    </div>
);

export default NavigationBar
*/
import React, { Component } from 'react';
import NavItem from "react-bootstrap/NavItem";
import { getIsLoggedInFromLocalStorage, setIsLoggedInLocalStorage, getRoleFromLocalStorage} from './Helpers';
import {Redirect} from "react-router";
import {
    Navbar,
    NavLink,
    NavbarBrand,
} from 'reactstrap';
import {Button} from "react-bootstrap";
import logo from './commons/images/icon.png';

function storeLoginStateInLocalStorage(state) {
    localStorage.setItem("state", state);
}

export default class NavigationBar extends Component {
    state = {
        loginIsVisible: false,
        isLoggedIn: getIsLoggedInFromLocalStorage(),
        activeRole: getRoleFromLocalStorage()};

    handleSelect = eventKey => {
        if (eventKey === 'logout') {
            setIsLoggedInLocalStorage('false');
            this.setState({isLoggedIn: 'false'});
            window.location=window.location.href;
        } else if (eventKey === 'login') {
            this.setState({loginIsVisible: true});
        }
    };

    render(){

        function handleLogout(event) {
            localStorage.clear();
            localStorage.removeItem('id');
            setIsLoggedInLocalStorage('false');
            document.location.reload();
            return <Redirect to="/" />
        }

        let loginNavLink;
        let homeNavLink;
        let viewPatientList;
        let viewCaregiverList;
        let viewMedicationList;
        const isLoggedIn = this.state.isLoggedIn;
        const activeRole=this.state.activeRole;
        if (isLoggedIn === 'true') {
            loginNavLink =  <form onSubmit={handleLogout}>
                <Button block bsSize="large"  type="submit"> Logout</Button>
            </form>;
            storeLoginStateInLocalStorage(this.state.isLoggedIn);
            if(activeRole==='doctor'){
                homeNavLink = <NavLink eventKey='homepage' className="text_navbar"  href="/doctorHome" onSelect={e => this.handleSelect(e)}>Home</NavLink>;
                viewPatientList = <NavLink eventKey='homepage' className="text_navbar"  href="/doctor/viewPatientList" onSelect={e => this.handleSelect(e)}>Patients</NavLink>;
                viewCaregiverList = <NavLink eventKey='homepage' className="text_navbar"  href="/doctor/viewCaregiverList" onSelect={e => this.handleSelect(e)}>Caregivers</NavLink>;
                viewMedicationList = <NavLink eventKey='homepage' className="text_navbar"  href="/doctor/viewMedicationList" onSelect={e => this.handleSelect(e)}>Medication</NavLink>;

            }
            if(activeRole==='patient'){
                homeNavLink = <NavLink eventKey='homepage' className="text_navbar"  href="/patientHome" onSelect={e => this.handleSelect(e)}>Home </NavLink>;
            }
            if(activeRole==='caregiver'){
                homeNavLink = <NavLink eventKey='homepage' className="text_navbar"  href="/caregiverHome" onSelect={e => this.handleSelect(e)}>Home </NavLink>;
                viewPatientList = <NavLink eventKey='homepage' className="text_navbar"  href="/caregiver/viewAllPatients" onSelect={e => this.handleSelect(e)}>Patients</NavLink>;

            }

        } else {
            loginNavLink = <NavLink eventKey='login' className="text_navbar"  href="/login" onSelect={e => this.handleSelect(e)}>Login</NavLink>;
        }
        console.log(this.state.isLoggedIn);
        return(
            <div>
                <Navbar className="color_navbar navbar-inverse navbar-fixed-top nav-tabs " light expand="md">
                    <NavbarBrand href="/">
                        <img src={logo} width={"50"}
                             height={"35"} />
                    </NavbarBrand>
                    <NavItem className="text_navbar">
                        {homeNavLink}
                    </NavItem>

                    <NavItem className="text_navbar">
                        {viewPatientList}
                    </NavItem>

                    <NavItem className="text_navbar">
                        {viewCaregiverList}
                    </NavItem>
                    <NavItem className="text_navbar">
                        {viewMedicationList}
                    </NavItem>
                    <NavItem className="text_navbar">
                        {loginNavLink}
                    </NavItem>
                </Navbar>
            </div>
        );
    }}


